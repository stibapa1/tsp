package cz.esw.profiling.ga;

import java.util.List;

/**
 * @author Marek Cuchý
 */
public interface Selection {

	/**
	 * It selects the given {@code number} of individuals from the {@code population}.
	 *
	 * @param population
	 * @param number
	 * @param <T>
	 * @return
	 */
	<T extends Individual> List<T> select(List<T> population, int number);
}

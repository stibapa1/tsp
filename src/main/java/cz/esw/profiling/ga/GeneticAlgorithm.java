package cz.esw.profiling.ga;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * @author Marek Cuchý
 */
public class GeneticAlgorithm<T extends Individual> {

	private final double mutationP;
	private final double elitism;
	private final int generations;

	private final Mutation<T> mutation;
	private final Crossover<T> crossover;
	private final Selection selection;

	private final Random random;

	public GeneticAlgorithm(double mutationP, double elitism, int generations, Mutation<T> mutation,
							Crossover<T> crossover, Selection selection, Random random) {
		this.mutationP = mutationP;
		this.elitism = elitism;
		this.generations = generations;
		this.mutation = mutation;
		this.crossover = crossover;
		this.selection = selection;
		this.random = random;
	}

	public T run(List<T> initialGeneration) {
		int n = initialGeneration.size();
		int elite = (int) (elitism * n);
		int generate = n - elite;

		T best = initialGeneration.get(0);
		List<T> currentGeneration = initialGeneration;
		for (int i = 0; i < generations; i++) {
			sort(currentGeneration);
			if (best.objective() > currentGeneration.get(0).objective()) {
				best = currentGeneration.get(0);
			}
			if (i % 100 == 0) {
				System.out.println(i + ": " + best);
			}

			List<T> nextGeneration = new ArrayList<>(n);

			//elitism
			nextGeneration.addAll(currentGeneration.subList(0, elite));

			//crossover
			List<T> parents = selection.select(currentGeneration, generate * 2);
			for (int j = 0; j < generate; j++) {
				T p1 = parents.get(2 * j);
				T p2 = parents.get(2 * j + 1);
				T offspring = crossover.perform(p1, p2);

				//mutation
				if (random.nextDouble() < mutationP) {
					offspring = mutation.mutate(offspring);
				}

				nextGeneration.add(offspring);
			}
			currentGeneration = nextGeneration;
		}

		return best;
	}

	private void sort(List<T> currentGeneration) {
		currentGeneration.sort(Comparator.comparingDouble(Individual::objective));
	}
}

package cz.esw.profiling.ga;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * This selection picks {@code k} random individuals from the population and returns the best one according the objective value.
 *
 * @author Marek Cuchý
 */
public class TournamentSelection implements Selection {

	private final Random random;
	private final int k;

	public TournamentSelection(Random random, int k) {
		this.random = random;
		this.k = k;
	}

	@Override
	public <T extends Individual> List<T> select(List<T> population, int number) {
		List<T> selection = new ArrayList<>(number);
		for (int i = 0; i < number; i++) {
			selection.add(tournament(population));
		}
		return selection;
	}

	/**
	 * Returns the best individual from {@code k} randomly picked.
	 *
	 * @param population
	 * @param <T>
	 * @return
	 */
	private <T extends Individual> T tournament(List<T> population) {
		return random.ints(k, 0, population.size()).mapToObj(population::get).min(
				Comparator.comparingDouble(Individual::objective)).get();
	}
}

package cz.esw.profiling;

import cz.esw.profiling.ga.*;
import cz.esw.profiling.tsp.SinglePointCrossover;
import cz.esw.profiling.tsp.SwapMutation;
import cz.esw.profiling.tsp.Tsp;
import cz.esw.profiling.tsp.TspIndividual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

/**
 * @author Marek Cuchý
 */
public class Main {

	public static final double MUTATION_P = 0.05;
	public static final double ELITISM = 0.05;
	public static final int POPULATION_SIZE = 1000;
	public static final int GENERATIONS = 1000;
	public static final int TOURNAMENT_K = 5;

	public static void main(String[] args) throws IOException {

		String fileName;
		if (args.length > 0) {
			fileName = args[0];
		} else {
			fileName = "data/pr439.tsp";
		}


		Random rnd = new Random(0);
		Tsp tsp1 = Tsp.loadTsp(fileName);

		//operators init
		Mutation<TspIndividual> mutation = new SwapMutation(rnd);
		Crossover<TspIndividual> crossover = new SinglePointCrossover(rnd);
		Selection selection = new TournamentSelection(rnd, TOURNAMENT_K);

		//algorithm init
		GeneticAlgorithm<TspIndividual> ga = new GeneticAlgorithm<>(MUTATION_P, ELITISM, GENERATIONS, mutation,
																	crossover, selection, rnd);
		List<TspIndividual> initialPopulation = createInitialPopulation(POPULATION_SIZE, tsp1, rnd);


		//algorithm run
		long t1 = System.currentTimeMillis();
		TspIndividual best = ga.run(initialPopulation);
		long t2 = System.currentTimeMillis();


		System.out.println("BEST: " + best);
		System.out.println("TIME: " + (t2 - t1) / 1000. + " s");
	}

	private static List<TspIndividual> createInitialPopulation(int n, Tsp tsp, Random rnd) {
		List<TspIndividual> population = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			List<Integer> list = IntStream.range(0, tsp.size()).boxed().collect(toList());
			Collections.shuffle(list, rnd);
			Integer[] ints = list.stream().toArray(Integer[]::new);
			population.add(new TspIndividual(tsp, ints));
		}
		return population;
	}

}

package cz.esw.profiling.tsp;

/**
 * @author Marek Cuchý
 */
public class City {

	public final int id;
	public final int x;
	public final int y;

	public City(int id, int x, int y) {
		this.id = id;
		this.x = x;
		this.y = y;
	}

	public double dist(City other) {
		int dx = x - other.x;
		int dy = y - other.y;
		return Math.sqrt(dx * dx + dy * dy);
	}

	@Override
	public String toString() {
		return "City [" + "id=" + id + ", x=" + x + ", y=" + y + ']';
	}
}

package cz.esw.profiling.tsp;

import cz.esw.profiling.ga.Individual;

import java.util.Arrays;

/**
 * @author Marek Cuchý
 */
public class TspIndividual implements Individual {

	private final Tsp tsp;

	/**
	 * Tour represented by IDs of the cities ordered by their position in the tour. Called 'path representation'.
	 */
	private final Integer[] order;

	public TspIndividual(Tsp tsp, Integer[] order) {
		this.tsp = tsp;
		this.order = order;
	}

	private double calculateObjective() {
		double sum = 0;
		for (int i = 1; i < order.length; i++) {
			int i1 = order[i - 1];
			int i2 = order[i];
			sum += tsp.dist(i1, i2);
		}
		sum += tsp.dist(order[order.length - 1], order[0]);
		return sum;
	}

	@Override
	public double objective() {
		return calculateObjective();
	}

	public Tsp getTsp() {
		return tsp;
	}

	public Integer[] getOrder() {
		return order;
	}

	@Override
	public String toString() {
		return "TspIndividual [objective=" + objective() + ", order=" + Arrays.toString(order) + ']';
	}
}

package cz.esw.profiling.tsp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Marek Cuchý
 */
public class Tsp {

	private final List<City> cities;

	/**
	 * Distance matrix between cities
	 */
	private final Double[][] dists;

	public Tsp(List<City> cities) {
		this.cities = cities;
		this.dists = initDists();
	}

	public double dist(int id1, int id2) {
		return cities.get(id1).dist(cities.get(id2));
	}

	private Double[][] initDists() {
		Double[][] dists = new Double[cities.size()][cities.size()];
		for (int i = 0; i < cities.size(); i++) {
			City c1 = cities.get(i);
			for (int j = 0; j < cities.size(); j++) {
				City c2 = cities.get(j);
				dists[i][j] = c1.dist(c2);
			}
		}
		return dists;
	}

	public int size(){
		return cities.size();
	}

	public static Tsp createRandomTsp(int size, int bound, Random rnd) {
		List<City> cities = new ArrayList<>(size);

		for (int i = 0; i < size; i++) {
			cities.add(new City(i, rnd.nextInt(bound), rnd.nextInt(bound)));
		}
		return new Tsp(cities);
	}

	public static Tsp loadTsp(String fileName) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		reader.readLine();
		reader.readLine();
		reader.readLine();
		String dimension = reader.readLine();
		reader.readLine();
		reader.readLine();

		int n = Integer.parseInt(dimension.split(":")[1].trim());

		List<City> cities = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			String line = reader.readLine();
			String[] split = line.split(" ");
			cities.add(new City(i, Integer.parseInt(split[1]), Integer.parseInt(split[2])));
		}
		reader.close();

		return new Tsp(cities);
	}
}

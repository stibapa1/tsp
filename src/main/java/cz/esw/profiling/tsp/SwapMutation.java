package cz.esw.profiling.tsp;

import cz.esw.profiling.ga.Mutation;

import java.util.Arrays;
import java.util.Random;

/**
 * Creates new individual by swapping two randomly selected cities.
 *
 * @author Marek Cuchý
 */
public class SwapMutation implements Mutation<TspIndividual> {

	private final Random rnd;

	public SwapMutation(Random rnd) {
		this.rnd = rnd;
	}

	@Override
	public TspIndividual mutate(TspIndividual origin) {
		Integer[] originalOrder = origin.getOrder();

		Integer[] mutant = Arrays.copyOf(originalOrder, originalOrder.length);

		int i1 = rnd.nextInt(originalOrder.length);
		int i2 = rnd.nextInt(originalOrder.length);
		while (i1 == i2) {
			i2 = rnd.nextInt(originalOrder.length);
		}

		swap(mutant, i1, i2);

		return new TspIndividual(origin.getTsp(), mutant);
	}

	private void swap(Integer[] array, int i1, int i2) {
		int tmp = array[i1];
		array[i1] = array[i2];
		array[i2] = tmp;
	}
}
